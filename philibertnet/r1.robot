*** Settings ***
Resource	philibert.resource
#Library		squash_tf.TFParamService

*** Variables ***
${url}    https://www.philibertnet.com/fr
${browser}    firefox

*** Keywords ***
Test Setup
	Open Browser    ${url}    ${browser}    service_log_path=${{os.path.devnull}}
    Maximize Browser Window
    Sleep    5
    ${refuser_button_present} =    Run Keyword And Return Status    Element Should Be Visible    xpath=//button[contains(text(),'Refuser')]
    Run Keyword If    ${refuser_button_present}    Click Element    xpath=//button[contains(text(),'Refuser')]
    
Test Teardown
	Sleep    5
    Close Browser


*** Test Cases ***
R1 - Rechercher rapidement un jeu (JDD1)
	
	[Setup]	Test Setup

	Given Je suis sur le site
	When Je recherche Jeu    ${Jdd1.jeu}
	Then Je vois le résultat de la recherche    ${Jdd1.jeu}
	When Je consulte un jeu
	Then Je vois tous les informations du jeu

	[Teardown]	Test Teardown

R1 - Rechercher rapidement un jeu (JDD2)
	
	[Setup]	Test Setup

	Given Je suis sur le site
	When Je recherche Jeu    ${Jdd2.jeu}
	Then Je vois le résultat de la recherche    ${Jdd2.jeu}
	When Je consulte un jeu
	Then Je vois tous les informations du jeu

	[Teardown]	Test Teardown

R1 - Rechercher rapidement un jeu (JDD3)
	
	[Setup]	Test Setup

	Given Je suis sur le site
	When Je recherche Jeu    ${Jdd3.jeu}
	Then Je vois le résultat de la recherche    ${Jdd3.jeu}
	When Je consulte un jeu
	Then Je vois tous les informations du jeu

	[Teardown]	Test Teardown