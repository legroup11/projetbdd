*** Settings ***
Resource	philibert.resource
#Library		squash_tf.TFParamService

*** Variables ***
${url}    https://www.philibertnet.com/fr
${browser}    firefox

*** Keywords ***
Test Setup
	Open Browser    ${url}    ${browser}    service_log_path=${{os.path.devnull}}
    Maximize Browser Window
    Sleep    5
    ${refuser_button_present} =    Run Keyword And Return Status    Element Should Be Visible    xpath=//button[contains(text(),'Refuser')]
    Run Keyword If    ${refuser_button_present}    Click Element    xpath=//button[contains(text(),'Refuser')]
    
Test Teardown
	Sleep    5
    Close Browser


*** Test Cases ***
A1 - Ajouter un nouveau jeu au panier
    ${jeu ajoute}=    Set Variable

	[Setup]	Test Setup

	Given Je suis sur le site
	When Je consulte les nouveaux jeux
	And J'ajoute un jeu le moins cher disponible au panier    
	Then Le jeu est ajouté au panier    ${jeu ajoute}    

	[Teardown]	Test Teardown