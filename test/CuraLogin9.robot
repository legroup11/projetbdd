*** Settings ***
Resource	CuraLogin9.resource

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox

*** Keywords ***
Test Setup
    Open Browser    ${url}    ${browser}    service_log_path=${{os.path.devnull}}
    Maximize Browser Window

Test Teardown
	Sleep    5
    Close Browser

*** Test Cases ***
Prendre un rendez-vous (JDD1)
	[Tags]    jdd1

	[Setup]	Test Setup

	Given L'utilisateur est connecté
	When L'utilisateur renseigne les informations obligatoires    ${Jdd1.facility}
	And L'utilisateur choisit HealthcareProgram    ${Jdd1.HealthcareProgram}
	And L'utilisateur clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram et le Facility sont affichés     ${Jdd1.verifHealthcareProgram}    ${Jdd1.facility}  

	[Teardown]	Test Teardown

Prendre un rendez-vous (JDD2)
	[Setup]	Test Setup

	Given L'utilisateur est connecté
	When L'utilisateur renseigne les informations obligatoires    ${Jdd2.facility}
	And L'utilisateur choisit HealthcareProgram    ${Jdd2.HealthcareProgram}
	And L'utilisateur clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram et le Facility sont affichés     ${Jdd2.verifHealthcareProgram}    ${Jdd2.facility}  

	[Teardown]	Test Teardown

Prendre un rendez-vous (JDD3)
	[Setup]	Test Setup

	Given L'utilisateur est connecté
	When L'utilisateur renseigne les informations obligatoires    ${Jdd3.facility}
	And L'utilisateur choisit HealthcareProgram    ${Jdd3.HealthcareProgram}
	And L'utilisateur clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram et le Facility sont affichés     ${Jdd3.verifHealthcareProgram}    ${Jdd3.facility}  

	[Teardown]	Test Teardown